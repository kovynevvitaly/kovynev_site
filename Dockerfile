#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["kovynev_site/kovynev_site.csproj", "kovynev_site/"]
RUN dotnet restore "kovynev_site/kovynev_site.csproj"
COPY . .
WORKDIR "/src/kovynev_site"
RUN dotnet build "kovynev_site.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "kovynev_site.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "kovynev_site.dll"]